import java.io.*;
import java.util.*;

public class footballdemo {
	public static void main(String[] args) throws IOException {
		// Step 1 - declare variables
		Scanner scan = null;
		int counter = 0;
		int LeedsScore = 0;
		int LiverpoolScore = 0;
		int HullScore = 0;
		int ArsenalScore = 0;
		int ChelseaScore = 0;
		int ManCityScore = 0;
		int totalScore = 0;
		boolean LeedsTrue = false;
		boolean LiverpoolTrue = false;
		boolean HullTrue = false;
		boolean ArsenalTrue = false;
		boolean ChelseaTrue = false;
		boolean ManCityTrue = false;
		ArrayList<String> arraylist = new ArrayList<String>();
		ArrayList<String> myList = new ArrayList<String>();

		// Step 2 - read in line by line and gather in a list
		try {
			scan = new Scanner(new BufferedReader(
					new FileReader("results1.txt")));

			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				arraylist.add(line);

				// Step 3 - read each item in the arraylist change : to ;
				// private static void processLine(String line) {

				Scanner data = new Scanner(line);
				data.useDelimiter(":");
				// provide data structure
				while (data.hasNext()) {
					String homeTeam = null;
					String awayTeam = null;
					String homeTeamScore = null;
					String awayTeamScore = null;
					int aTScore;
					int hTScore;
					boolean isValid = true;

					homeTeam = data.next().trim();
					// System.out.println("Home Team is " + homeTeam);
					if (homeTeam.length() == 0) {
						isValid = false;
					}

					if (data.hasNext()) {
						awayTeam = data.next().trim();
						// System.out.println("Away Team is " + awayTeam);
						if (awayTeam.length() == 0) {
							isValid = false;
						}

						if (data.hasNext()) {
							homeTeamScore = data.next().trim();
							// System.out.println("Home Team score is " +
							// homeTeamScore);
							if (homeTeamScore.length() == 0) {
								isValid = false;
							} else {
								try {
									hTScore = Integer.parseInt(homeTeamScore);
									// System.out.println("hTScore is " +
									// hTScore);
								} catch (NumberFormatException e) {
									isValid = false;
									// System.out.println("is Valid =  " +
									// isValid);
								}
							}

							if (data.hasNext()) {
								awayTeamScore = data.next().trim();
								// System.out.println("Away Team score is " +
								// awayTeamScore);
								try {
									aTScore = Integer.parseInt(awayTeamScore);
									// System.out.println("aTScore is " +
									// aTScore);
								} catch (NumberFormatException e) {
									isValid = false;
									// System.out.println("is Valid =  " +
									// isValid);
								}

							} else {
								// System.out.println("No more data");
								isValid = false;
							}
						}

					}

					if (isValid == true) {
						System.out.println("is Valid = " + isValid
								+ "  Home Team is " + homeTeam
								+ "  awayTeam is " + awayTeam
								+ "  Home Team Score is " + homeTeamScore
								+ "  Away Team Score is " + awayTeamScore);
						counter++;

						myList.add(homeTeam);
						myList.add(homeTeamScore);
						myList.add(awayTeam);
						myList.add(awayTeamScore);

					} else {
						// System.out.println ("is Valid = " + isValid);
					}

				}

				data.close();

			}
			System.out.println(myList);
			for (int i = 0; i < myList.size(); i += 2) {
				String team = myList.get(i);
				switch (team) {
				case "Leeds United":
					LeedsScore = LeedsScore
							+ Integer.parseInt(myList.get(i + 1));
					// System.out.println("Leeds United Score " + LeedsScore );
					LeedsTrue = true;
					break;

				case "Liverpool":
					LiverpoolScore = LiverpoolScore
							+ Integer.parseInt(myList.get(i + 1));
					// System.out.println("Liverpool Score " + LiverpoolScore);
					LiverpoolTrue = true;
					break;

				case "Chelsea":
					ChelseaScore = ChelseaScore
							+ Integer.parseInt(myList.get(i + 1));
					// System.out.println("Chelsea Score " + ChelseaScore);
					ChelseaTrue = true;
					break;

				case "Manchester City":
					ManCityScore = ManCityScore
							+ Integer.parseInt(myList.get(i + 1));
					// System.out.println("Manchester City Score " +
					// ManCityScore);
					ManCityTrue = true;
					break;

				case "Hull":
					HullScore = HullScore + Integer.parseInt(myList.get(i + 1));
					// System.out.println("Hull Score " + HullScore);
					HullTrue = true;
					break;

				case "Arsenal":
					ArsenalScore = ArsenalScore
							+ Integer.parseInt(myList.get(i + 1));
					// System.out.println("Arsenal Score " + ArsenalScore);
					ArsenalTrue = true;
					break;

				}
			}
			if (LeedsTrue == true) {
				System.out.println("Leeds Score is " + LeedsScore);
				totalScore = totalScore + LeedsScore;
			}
			if (LiverpoolTrue == true) {
				System.out.println("Liverpool Score is " + LiverpoolScore);
				totalScore = totalScore + LiverpoolScore;
			}
			if (ChelseaTrue == true) {
				System.out.println("Chelsea Score is " + ChelseaScore);
				totalScore = totalScore + ChelseaScore;
			}
			if (ManCityTrue == true) {
				System.out.println("Manchester City Score is " + ManCityScore);
				totalScore = totalScore + ManCityScore;
			}
			if (HullTrue == true) {
				System.out.println("Hull Score is " + HullScore);
				totalScore = totalScore + HullScore;
			}
			if (ArsenalTrue == true) {
				System.out.println("Arsenal Score is " + ArsenalScore);
				totalScore = totalScore + ArsenalScore;
			}

			System.out.println("The total score : " + totalScore);
			System.out.println("The number of valid records are: " + counter);
			System.out.println("The number of invalid records are "
					+ (arraylist.size() - counter));

		} finally {
			scan.close();

		}
	}

}